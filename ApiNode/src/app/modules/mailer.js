const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')

const { host, port, user, pass } = require('../../config/main.json')

const transport = nodemailer.createTransport({
	host,
	port,
	auth: { user, pass },
})

module.exports = transport
