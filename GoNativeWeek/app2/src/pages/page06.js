import React, { Component } from 'react';
import { View, StyleSheet, Text, BackAndroid } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class Page06 extends Component {

  render() {
    return (
      <View style={styles.viewCenter}>
        <Text>Page 06</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewCenter: {
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  }
})

