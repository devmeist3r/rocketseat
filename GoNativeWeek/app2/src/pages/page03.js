import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Animated,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const width = Dimensions.get('screen').width;

export default class Page03 extends Component {
    state = {
        ballPosY: new Animated.Value(0)
    };

    animatedFuncPlus = () => {
        Animated.timing(this.state.ballPosY, {
            toValue: 400,
            duration: 500
        }).start();
    };

    animatedFuncMinus = () => {
        Animated.timing(this.state.ballPosY, {
            toValue: 0,
            duration: 500
        }).start();
    };

    render() {
        return (
            <View style={styles.viewCenter}>
                <View style={styles.viewTop}>
                    <View style={styles.viewTop2}>
                        <TouchableOpacity
                            style={styles.viewCenter1}
                            onPress={this.animatedFuncMinus.bind()}
                        >
                            <Ionicons name={'ios-remove-outline'} size={50} color={'blue'}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.viewCenter1}
                            onPress={this.animatedFuncPlus.bind()}
                        >
                            <Ionicons name={'ios-add-outline'} size={50} color={'blue'}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.viewBall}>
                    <Animated.View
                        style={[styles.ball, {marginTop: this.state.ballPosY}]}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewCenter: {
        flex: 1,
        backgroundColor: 'white'
    },
    viewTop: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    viewCenter1: {
        height: 50,
        width: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTop2: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: width,
        height: 50
    },
    viewBall: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    ball: {
        width: 60,
        height: 60,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    }
});
