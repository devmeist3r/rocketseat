import React, { Component } from 'react';
import { View, StyleSheet, Text, AsyncStorage } from 'react-native';

export default class Page02 extends Component {
  state = {
    nome: ''
  };

  async componentDidMount() {
    //Apenas Salvar no banco de dados
    //AsyncStorage.setItem('@GoNative:nome', 'Lucas');

    const nome = await AsyncStorage.getItem('@GoNative:nome');
    this.setState({ nome });
  }

  render() {
    return (
      <View style={styles.viewCenter}>
        <Text>Meu nome é: {this.state.nome}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  }
});
