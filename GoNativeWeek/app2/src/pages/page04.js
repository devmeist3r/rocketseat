import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Share
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class Page04 extends Component {
  
  state = {
    title: '',
    message: ''
  }

  shareMessage = () => {
    Share.share({
      title: 'GoNative Week',
      message: 'Essa Semana da GoNative Week está sendo demais'
    });
  }

  render() {
    return (
      <View style={styles.viewCenter}>
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={() => this.props.navigation.navigate('Page05')}
        >
          <Text style={styles.buttonText}>Go to Page 05</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.buttonStyle, { marginTop: 10 }]}
          onPress={this.shareMessage.bind()}
        >
          <Text style={styles.buttonText}>Share</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  buttonStyle: {
    backgroundColor: 'black',
    height: 50,
    width: 200,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white',
    fontSize: 16
  }
});
