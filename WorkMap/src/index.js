import React, { Component } from 'react'
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  View,
  Dimensions,
} from 'react-native'
import MapView from 'react-native-maps'

const { height, width } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },

  mapView: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  placesContainer: {
    width: '100%',
    maxHeight: 200,
  },

  place: {
    width: width - 40,
    maxHeight: 200,
    backgroundColor: '#FFF',
    marginHorizontal: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: 20,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 18,
    backgroundColor: 'transparent',
  },

  description: {
    color: '#999',
    fontSize: 12,
    marginTop: 5,
  },
})

export default class App extends Component {
  state = {
    latitude: -27.210671,
    longitude: -49.63627,
  }

  componentDidMount() {
    setTimeout(() => {
      this.mapView.animateToCoordinate(
        {
          latitude: -27.260671,
          longitude: -49.69627,
        },
        2000,
      )
    }, 6000)
  }

  render() {
    const { latitude, longitude } = this.state
    return (
      <SafeAreaView style={styles.container}>
        <MapView
          ref={map => {
            this.mapView = map
          }}
          initialRegion={{
            latitude,
            longitude,
            latitudeDelta: 0.0042,
            longitudeDelta: 0.0031,
          }}
          style={styles.mapView}
          rotateEnabled={false}
          scrollEnabled={false}
          zoomEnabled={false}
          showsPointsOfInterest={false}
          showBuildings={false}
        >
          <MapView.Marker
            coordinate={{
              latitude: -27.210671,
              longitude: -49.63627,
            }}
          />
        </MapView>
        <ScrollView
          sytle={styles.placesContainer}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
        >
          <View style={styles.place} />
          <View style={styles.place} />
          <View style={styles.place} />
        </ScrollView>
      </SafeAreaView>
    )
  }
}
