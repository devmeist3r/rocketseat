import Reactotron from 'reactotron-react-native'

const tron = Reactotron
  .configure()
  .useReactNative()
  .connect()
  .configure({ host: '192.168.0.0' })

tron.clear()

console.tron = tron
