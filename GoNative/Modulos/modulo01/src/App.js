import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Todo from 'components/Todo';
import ReactotronConfig from './Config/ReactotronConfig';
import DevToolsConfig from './Config/DevToolsConfig';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      usuario: 'Lucas',
      todos: [
        { id: 0, text: 'Fazer café' },
        { id: 1, text: 'Estudar GoNative' },
      ],
    };
  }

  componentDidMount() {
    console.tron.log('teste');
  }

  addTodo = () => {
    this.setState({
      todos: [
        ...this.state.todos,
        { id: Math.random(), text: 'Estudar javascript' }
      ]
    });
  };

  render() {
    return (
      <View style={styles.container}>
        {/* <Todo title="Fazer café"/>
        <Todo title={5}/> */}
        <Text>{this.state.usuario}</Text>
        {this.state.todos.map(todo => <Todo title={todo.text} key={todo.id} />)}
        <Button title="Adicionar todo" onPress={this.addTodo} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  }
});
