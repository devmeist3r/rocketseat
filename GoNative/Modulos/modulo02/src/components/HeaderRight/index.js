import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, AsyncStorage } from 'react-native';
import { NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';

export default class MyComponent extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      dispatch: PropTypes.func,
    }).isRequired,
  };

  signOut = async () => {
    await AsyncStorage.clear();
    const { dispatch } = this.props.navigation;

    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Welcome' })],
    });

    dispatch(resetAction);
  };

  render() {
    return (
      <TouchableOpacity onPress={this.signOut}>
        <Icon name="logout" size={24} style={styles.icon} />
      </TouchableOpacity>
    );
  }
}
