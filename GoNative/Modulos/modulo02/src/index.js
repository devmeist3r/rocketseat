import React, { Component } from 'react'
import 'config/ReactotronConfig'
import { SafeAreaView } from 'react-navigation'
import { AsyncStorage } from 'react-native'
import createNavigator from 'routes'
import { general } from 'styles'

export default class App extends Component {
  state = {
    userChecked: false,
    userLogged: false,
  }

  async componentDidMount() {
    const username = await AsyncStorage.getItem('@Githuber:username')

    this.appLoaded(username)
  }

  appLoaded = username => {
    this.setState({
      userChecked: true,
      userLogged: !!username,
    })
  }

  render() {
    if (!this.state.userChecked) return null

    const Routes = createNavigator(this.state.userLogged)

    return (
      <SafeAreaView style={general.safeArea}>
        <Routes />
      </SafeAreaView>
    )
  }
}
