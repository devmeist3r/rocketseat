import axios from 'axios'

const api = axios.create({
  baseURL: 'http://localhost:3000',
  // baseURL: 'http://172.27.0.187:3000',
})

export default api
