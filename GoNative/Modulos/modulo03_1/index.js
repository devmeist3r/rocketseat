import { AppRegistry } from 'react-native'
import App from './src'

console.disableYellowBox = true

AppRegistry.registerComponent('mod03', () => App)
