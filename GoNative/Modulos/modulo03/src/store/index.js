import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'

const middleware = [];

const createApppropriateStore = __DEV__ ? console.tron.createStore : createStore
const store = createApppropriateStore(reducers, applyMiddleware(...middleware))

export default store
