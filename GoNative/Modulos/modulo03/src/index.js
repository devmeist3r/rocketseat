import React from 'react';
import { } from 'react-native';
import 'config/ReactotronConfig';
import { Provider } from 'react-redux';
import store from 'store';
import TodoList from './TodoList';

// import styles from './styles';

const App = () => (
  <Provider store={store}>
    <TodoList />
  </Provider>
);

export default App;
