import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import styles from './style';
import Header from './Components/Header/index';
import Post from './Components/Post/index';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [
        {
          id: 1,
          title: 'Aprendendo React Native',
          author: 'Diego Schell Fernandes',
          message:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        },
        {
          id: 2,
          title: 'Aprendendo React Native',
          author: 'Diego Schell Fernandes',
          message:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        },
        {
          id: 3,
          title: 'Aprendendo React Native',
          author: 'Diego Schell Fernandes',
          message:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
        },
      ],
    };
  }

  render() {
    return (
      <View style={styles.main}>
        <Header />
        <View style={styles.viewCenter}>
          <ScrollView styles={styles.contentContainer}>
            {this.state.posts.map(post => (
              <Post
                title={post.title}
                author={post.author}
                message={post.message}
                key={post.id}
              />
            ))}
          </ScrollView>
        </View>
      </View>
    );
  }
}
