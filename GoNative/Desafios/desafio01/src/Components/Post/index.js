import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const Post = ({ title, author, message }) => (
  <View style={styles.main}>
    <View style={styles.viewHeader}>
      <Text style={styles.textTitle}>{title}</Text>
    </View>
    <View style={styles.viewCenter}>
      <Text style={styles.textCenter}>{author}</Text>
    </View>
    <View style={styles.viewBottom}>
      <Text style={styles.textBottom}>{message}</Text>
    </View>
  </View>
);

Post.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

export default Post;
