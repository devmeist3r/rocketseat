import React, { Component } from 'react';
import { View, StyleSheet, Text, BackAndroid } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class Page05 extends Component {

  static navigationOptions = ({navigation}) => {
    return {
        title: "Page05",
        headerStyle: {backgroundColor: "#f3f3f3"},
    };
};

  render() {
    return (
      <View style={styles.viewCenter}>
        <Text>Page 05</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewCenter: {
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  }
})

