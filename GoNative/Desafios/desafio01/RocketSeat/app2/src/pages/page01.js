import React, { Component } from 'react';
import { View, StyleSheet, Text, BackAndroid } from 'react-native';

export default class Page01 extends Component {

  state = {
    latitude: 0,
    longitude: 0
  };

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(position => {
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      });
    });
  }

  render() {
    return (
      <View style={styles.viewCenter}>
        <Text>Sua latitude é: {this.state.latitude}</Text>
        <Text>Sua longitude é: {this.state.longitude}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  viewCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  }
})

