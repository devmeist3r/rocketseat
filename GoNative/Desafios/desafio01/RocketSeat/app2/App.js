import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { TabNavigator, TabBarBottom, StackNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Page01 from './src/pages/page01';
import Page02 from './src/pages/page02';
import Page03 from './src/pages/page03';
import Page04 from './src/pages/page04';
import Page05 from './src/pages/page05';
import Page06 from './src/pages/page06';

const Page01Stack = {
  Page01: {
    screen: Page01,
    navigationOptions: {
      title: 'Localização'
    }
  }
};
const Page01Router = StackNavigator(Page01Stack);

const Page02Stack = {
  Page02: {
    screen: Page02,
    navigationOptions: {
      title: 'Salvar dados'
    }
  }
};
const Page02Router = StackNavigator(Page02Stack);

const Page03Stack = {
  Page03: {
    screen: Page03,
    navigationOptions: {
      title: 'Animação'
    }
  }
};
const Page03Router = StackNavigator(Page03Stack);


const Page04Stack = {
  Page04: {
    screen: Page04,
    navigationOptions: {
      title: 'Page 04'
    }
  },
  Page05: {
    screen: Page05
  }
};
const Page04Router = StackNavigator(Page04Stack);

const Page06Stack = {
  Page06: {
    screen: Page06,
    navigationOptions: {
      title: 'Menu'
    }
  }
};
const Page06Router = StackNavigator(Page06Stack);

var App = TabNavigator(
  {
    Page01: {
      screen: Page01Router,
      navigationOptions: {
        tabBarLabel: 'Mapas',
        tabBarIcon: ({ tintColor }) => {
          return <Ionicons name={'ios-map'} size={25} color={'blue'} />;
        }
      }
    },
    Page02: {
      screen: Page02Router,
      navigationOptions: {
        tabBarLabel: 'Save',
        tabBarIcon: ({ tintColor }) => {
          return (
            <Ionicons name={'ios-cloud-outline'} size={25} color={'blue'} />
          );
        }
      }
    },
    Page03: {
      screen: Page03Router,
      navigationOptions: {
        tabBarLabel: 'Anime',
        tabBarIcon: ({ tintColor }) => {
          return <Ionicons name={'ios-game-controller-a-outline'} size={25} color={'blue'} />;
        }
      }
    },
    Page04: {
      screen: Page04Router,
      navigationOptions: {
        tabBarLabel: 'Page',
        tabBarIcon: ({ tintColor }) => {
          return (
            <Ionicons name={'ios-copy-outline'} size={25} color={'blue'} />
          );
        }
      }
    },
    Page06: {
      screen: Page06Router,
      navigationOptions: {
        tabBarLabel: 'Menu',
        tabBarIcon: ({ tintColor }) => {
          return (
            <Ionicons name={'ios-menu-outline'} size={25} color={'blue'} />
          );
        }
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: 'black',
      activeBackgroundColor: '#C0C0C0',
      inactiveTintColor: '#696969',
      inactiveBackgroundColor: '#DCDCDC',
      labelStyle: {
        fontSize: 12
      }
    },
    // tabBarPosition: Platform.OS === 'ios'? 'bottom': 'top',
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false
  }
);

App.navigationOptions = {
  title: 'Tab example'
};

export default App;
