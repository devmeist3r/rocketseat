export default {
  main: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    backgroundColor: 'white',
    paddingTop: 20,
  },
  textHeader: {
    fontSize: 18,
  },
};
