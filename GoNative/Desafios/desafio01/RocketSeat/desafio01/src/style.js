export default {
  main: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#EE7777',
  },
  viewCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 24,
  },
  instructions: {
    fontSize: 22,
    color: 'blue',
  },
  contentContainer: {
    paddingVertical: 20,
  },
};
