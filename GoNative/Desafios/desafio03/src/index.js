import React from 'react'
import { Provider } from 'react-redux'
import 'config/ReactotronConfig'
import store from 'store'
import MView from 'components/mapView'

const App = () => (
  <Provider store={store}>
    <MView />
  </Provider>
)

export default App
