import React, { Component } from 'react'
import { SafeAreaView, View, Image } from 'react-native'
import MapView from 'react-native-maps'
import styles from './styles'
import MyModal from './components'

export default class MView extends Component {
  state = {
    latitude: -27.2177659,
    longitude: -49.6451598,
    showModal: false,
  }

  handleLongPress = e => {
    this.setState({ showModal: true, coordinate: e.coordinate })
  }

  render() {
    const { latitude, longitude } = this.state
    return (
      <SafeAreaView style={styles.container}>
        <MapView
          ref={map => {
            this.mapView = map
          }}
          initialRegion={{
            latitude,
            longitude,
            latitudeDelta: 0.0042,
            longitudeDelta: 0.0031,
          }}
          style={styles.mapView}
          onLongPress={event => {
            this.handleLongPress(event.nativeEvent)
          }}
        />
        <MyModal
          visible={this.state.showModal}
          coordinate={this.state.coordinate}
          onCloseModal={() =>
            this.setState({ showModal: false, coordinate: {} })
          }
        />
      </SafeAreaView>
    )
  }
}
