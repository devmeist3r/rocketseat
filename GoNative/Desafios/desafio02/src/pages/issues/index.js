import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native'
import PropTypes from 'prop-types'
import api from 'services/api'
import styles from './styles'
import IssueItem from './components'

export default class Issues extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.repositorie.name,
  })

  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      state: PropTypes.object,
    }).isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      data: [],
      loading: false,
      refreshing: false,
      repoState: 'all',
    }
  }

  componentDidMount = async () => {
    const filterRepo = await AsyncStorage.getItem('@FilterRepo:')
    const { repositorie } = this.props.navigation.state.params
    this.loadIssues(repositorie)
    this.setState({
      repoState: filterRepo,
    })
    console.tron.log(filterRepo)
  }

  loadIssues = async repositorie => {
    this.setState({ loading: true })

    try {
      const issues = await api.get(
        `/repos/${repositorie.owner.login}/${repositorie.name}/issues?state=${
          this.state.repoState
        }`,
      )
      this.setState({
        data: issues.data,
        loading: false,
        refreshing: false,
      })
      console.tron.log(issues)
    } catch (err) {
      this.setState({ loading: false })
    }
  }

  stateRepoLoad = state => {
    const { repositorie } = this.props.navigation.state.params
    this.setState({
      repoState: state,
    })
    AsyncStorage.setItem('@FilterRepo:', state)
    this.loadIssues(repositorie)
  }

  renderListItem = ({ item }) => <IssueItem issue={item} />

  renderList = () => (
    <FlatList
      data={this.state.data}
      keyExtractor={item => String(item.id)}
      renderItem={this.renderListItem}
      refreshing={this.state.refreshing}
      onRefresh={this.loadIssues}
    />
  )

  render() {
    return (
      <View sytle={styles.container}>
        <View style={styles.tabTop}>
          <TouchableOpacity onPress={() => this.stateRepoLoad('all')}>
            <Text
              style={
                this.state.repoState === 'all'
                  ? styles.activity
                  : styles.desable
              }
            >
              Todas
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.stateRepoLoad('open')}>
            <Text
              style={
                this.state.repoState === 'open'
                  ? styles.activity
                  : styles.desable
              }
            >
              Abertas
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.stateRepoLoad('closed')}>
            <Text
              style={
                this.state.repoState === 'closed'
                  ? styles.activity
                  : styles.desable
              }
            >
              Fechadas
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.viewFlat}>
          {this.state.loading ? (
            <ActivityIndicator style={styles.loading} />
          ) : (
            this.renderList()
          )}
        </View>
      </View>
    )
  }
}
