import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Image, Text, TouchableOpacity, Linking } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

import styles from './styles'

export default class IssueItem extends Component {
  static propTypes = {
    issue: PropTypes.shape({
      html_url: PropTypes.string,
      title: PropTypes.string,
      user: PropTypes.shape({
        avatar_url: PropTypes.string,
        login: PropTypes.string,
      }).isRequired,
    }).isRequired,
  }

  openIssue = () => {
    Linking.openURL(this.props.issue.html_url)
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.repoImage}
          source={{ uri: this.props.issue.user.avatar_url }}
        />
        <View style={styles.infoContainer}>
          <Text numberOfLines={1} style={styles.repoTitle}>
            {this.props.issue.title}
          </Text>
          <Text style={styles.organization}>{this.props.issue.user.login}</Text>
        </View>
        <TouchableOpacity onPress={this.openIssue}>
          <Icon
            name="angle-right"
            size={20}
            style={styles.addIcon}
            color="#999"
          />
        </TouchableOpacity>
      </View>
    )
  }
}
