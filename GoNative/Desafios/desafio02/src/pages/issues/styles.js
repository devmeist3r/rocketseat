import { StyleSheet } from 'react-native'
import { colors, metrics } from 'styles'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: metrics.basePadding,
  },
  tabTop: {
    marginTop: metrics.baseMargin,
    height: 40,
    flexDirection: 'row',
    backgroundColor: colors.boxFilter,
    borderRadius: metrics.borderRadius,
    marginHorizontal: 20,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  issuesContainer: {
    marginTop: metrics.baseMargin / 2,
  },
  loading: {
    marginTop: metrics.basePadding,
  },
  viewFlat: {
    marginTop: metrics.basePadding,
  },
  activity: {
    color: colors.black,
    fontWeight: 'bold',
  },
  disable: {
    color: '#555',
  },
})

export default styles
