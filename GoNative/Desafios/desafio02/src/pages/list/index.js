import React, { Component } from 'react'
import {
  View,
  Alert,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native'
import api from 'services/api'
import PropTypes from 'prop-types'
import Header from 'components/header'
import styles from './styles'
import RepositoryItem from './components/'

export default class List extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: <Header {...navigation.state.params} />,
    }
  }

  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      setParams: PropTypes.func,
    }).isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      data: [],
      loading: false,
      refreshing: false,
    }
  }

  componentDidMount() {
    this.loadRepositories()
  }

  loadRepositories = async () => {
    this.setState({ refreshing: true })
    const repos = await AsyncStorage.getItem('@RepoList:data')
    if (repos !== null) {
      this.setState({ data: JSON.parse(repos), loading: false })
    }
    this.props.navigation.setParams({
      saveRepo: this.saveRepo,
    })
    this.setState({ loading: false, refreshing: false })
  }

  checkRepoExists = async searchInput => {
    const repo = await api.get(`/repos/${searchInput}`)
    return repo
  }

  saveRepo = async searchInput => {
    if (searchInput.length === 0) return

    this.setState({ loading: true })

    try {
      await this.checkRepoExists(searchInput)

      const response = await api.get(`/repos/${searchInput}`)
      await this.setState({
        data: [...this.state.data, response.data],
        loading: false,
        refreshing: false,
      })
      await AsyncStorage.setItem(
        '@RepoList:data',
        JSON.stringify(this.state.data),
      )
      console.tron.log('funfou')
    } catch (err) {
      this.setState({
        loading: false,
      })
      Alert.alert(
        'Erro 404',
        `Repositório \n ${searchInput} \n não encontrado!`,
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false },
      )
    }
  }

  renderListItem = ({ item }) => (
    <RepositoryItem
      repositorie={item}
      navigation={this.props.navigation}
    />
  )

  renderList = () => (
    <FlatList
      data={this.state.data}
      keyExtractor={item => String(item.id)}
      renderItem={this.renderListItem}
      refreshing={this.state.refreshing}
      onRefresh={this.loadRepositories}
    />
  )

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? (
          <ActivityIndicator style={styles.loading} />
        ) : (
          this.renderList()
        )}
      </View>
    )
  }
}
