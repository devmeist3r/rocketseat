import React, { Component } from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types'
import styles from './styles'

export default class RepositoryItem extends Component {
  viewIssues = () => {
    const { repositorie } = this.props
    this.props.navigation.navigate('Issues', { repositorie })
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.repoImage}
          source={{ uri: this.props.repositorie.owner.avatar_url }}
        />
        <View style={styles.infoContainer}>
          <Text style={styles.repoTitle}>{this.props.repositorie.name}</Text>
          <Text style={styles.infoText}>
            {this.props.repositorie.owner.login}
          </Text>
        </View>
        <TouchableOpacity onPress={this.viewIssues}>
          <Icon name="angle-right" size={30} style={styles.infoIcon} />
        </TouchableOpacity>
      </View>
    )
  }
}

RepositoryItem.propTypes = {
  repositorie: PropTypes.shape({
    name: PropTypes.string,
    avatar_url: PropTypes.string,
    owner: PropTypes.shape({
      login: PropTypes.string,
      avatar_url: PropTypes.string,
    }).isRequired,
  }).isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
}
