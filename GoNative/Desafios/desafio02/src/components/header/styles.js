import { StyleSheet } from 'react-native';
import { colors } from 'styles';

const styles = StyleSheet.create({
  container: {
    height: 70,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.secundary,
    paddingTop: 30,
    paddingBottom: 10,
    backgroundColor: '#f7f7f7',
  },
  form: {
    width: '85%',
  },
  textInput: {
    backgroundColor: colors.boxFilter,
    borderRadius: 3,
    height: 34,
    paddingHorizontal: 10,
    fontSize: 12,
  },
  button: {
    marginLeft: 10,
  },
});

export default styles;
