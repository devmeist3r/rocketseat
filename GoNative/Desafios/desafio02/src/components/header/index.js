import React, { Component } from 'react'
import { View, TextInput, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import 'config/ReactotronConfig'
import styles from './styles'

export default class Header extends Component {
  static defaultProps = {
    saveRepo: () => {},
  }

  constructor(props) {
    super(props)
    this.state = {
      searchInput: '',
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.form}>
          <TextInput
            style={styles.textInput}
            placeholder="Adicionar repositório"
            underlineColorAndroid="transparent"
            value={this.state.searchInput}
            onChangeText={searchInput => this.setState({ searchInput })}
            autoCapitalize="none"
          />
        </View>
        <View style={styles.button}>
          <TouchableOpacity
            onPress={() => this.props.saveRepo(this.state.searchInput)}
          >
            <Icon name="plus" size={16} color="#000" />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
