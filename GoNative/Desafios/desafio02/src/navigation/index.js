import React from 'react';
import { SafeAreaView } from 'react-navigation';
import Routes from './routes';
import styles from './styles';

const App = () => (
  <SafeAreaView style={styles.safeArea}>
    <Routes />
  </SafeAreaView>
);

export default App;
