export default {
  primary: '#EEEEEE',
  secundary: '#FFFFFF',
  title: '#333333',
  description: '#999999',
  boxFilter: '#DDDDDD',
  colorFilter: '#666666',
  transparent: 'trasparent',
  darkTrasparent: 'rgba(0, 0, 0, 0.5)',
  whiteTransparent: 'rgba(255, 255, 255, 0.3)',
  header: '#f7f7f7',
  black: '#000',
  desable: '#555',
};
