import React, { Component } from 'react'
import { StyleSheet, View, Animated } from 'react-native'

// Animated.View,
// Animated.Text,
// Animated.Image,
// Animated.ScrollView

const ballY = new Animated.Value(0)
const ballX = new Animated.divide(ballY, 2)

export default class App extends Component {
  state = {
    ballY: ballY,
    ballX: ballX,
  }

  // timing - tempo q ele leva para animar
  // componentDidMount() {
  //   Animated.timing(this.state.ballY, {
  //     toValue: 500,
  //     duration: 1000,
  //   }).start()
  // }

  // spring - animaçao elastico
  // componentDidMount() {
  //   Animated.spring(this.state.ballY, {
  //     toValue: 500,
  //     bounciness: 30,
  //   }).start()
  // }

  // decay - acelaraçao e freio de carro
  componentDidMount() {
    Animated.decay(this.state.ballY, {
      velocity: 1,
    }).start()
  }

  render() {
    return (
      <View style={styles.container}>
        <Animated.View
          style={[
            styles.ball,
            { top: this.state.ballY, left: this.state.ballX },
          ]}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
  },
  ball: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: '#f00',
  },
})
